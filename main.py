import random

import player_options


def pick_race():
    picked_race = random.choice(player_options.player_race)
    return picked_race


def pick_class():
    picked_class = random.choice(player_options.player_class)
    return picked_class


def pick_background():
    picked_background = random.choice(list(player_options.player_background))
    picked_feature = player_options.player_background.get(picked_background, 0)
    p_bg_f = [picked_background, picked_feature]
    return p_bg_f


def pick_ability_scores():
    ability = ['Strength: ', 'Dexterity: ', 'Constitution: ',
               'Intelligence: ', 'Wisdom: ', 'Charisma: ']
    scores = [int(x) for x in random.sample(range(8, 16), 6)]
    a_scores = dict(zip(ability, scores))

    return a_scores


def pick_ability_modifier():
    ability = ['Strength: ', 'Dexterity: ', 'Constitution: ',
               'Intelligence: ', 'Wisdom: ', 'Charisma: ']
    modifiers = list()
    for x in ability:
        modifiers = player_options.player_modifiers[x]

    a_modifiers = dict(zip(ability, modifiers))

    return a_modifiers


print("Race: %s" % pick_race())
print("Class: %s" % pick_class())

background_feature = pick_background()
print("Background: %s\n    - Feature: %s\n" % (background_feature[0], background_feature[1]))

ability_scores = pick_ability_scores()
print("Strength: %d\n"
      "Dexterity: %d\n"
      "Constitution: %d\n"
      "Intelligence: %d\n"
      "Wisdom: %d\n"
      "Charisma: %d\n" % (ability_scores['Strength: '], ability_scores['Dexterity: '],
                          ability_scores['Constitution: '], ability_scores['Intelligence: '],
                          ability_scores['Wisdom: '], ability_scores['Charisma: ']))

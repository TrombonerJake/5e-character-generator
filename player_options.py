player_class = ['Artificer', 'Barbarian', 'Bard', 'Cleric', 'Druid', 'Fighter',
                'Monk', 'Paladin', 'Ranger', 'Rogue', 'Sorcerer', 'Warlock', 'Wizard']

player_race = ['Aarakocra', 'Aasimar', 'Bugbear', 'Centaur', 'Changeling', 'Dhampir',
               'Dragonborn', 'Dwarf', 'Elf', 'Fairy', 'Firbolg', 'Air Genasi', 'Earth Genasi',
               'Fire Genasi', 'Water Genasi', 'Gith', 'Gnome', 'Goblin', 'Goliath', 'Half-Elf',
               'Half-Orc', 'Halfling', 'Harengon', 'Hexblood', 'Hobgoblin', 'Human', 'Kalashtar',
               'Kenku', 'Kobold', 'Leonin', 'Lizardfolk', 'Loxodon', 'Minotaur', 'Orc', 'Owlin',
               'Reborn', 'Satyr', 'Shifter', 'Simic Hybrid', 'Tabaxi', 'Tiefling', 'Tortle',
               'Triton', 'Vedalken', 'Verdan', 'Warforged', 'Yuan-Ti']

player_background = {'Acolyte': 'Shelter of the Faithful', 'Anthropologist': 'Adept Linguist',
                     'Archaeologist': 'Historical Knowledge', 'Charlatan': 'False Identity',
                     'City Watch': 'Watcher\'s Eye', 'Clan Crafter': 'Respect of the Stout Folk',
                     'Courtier': 'Court Functionary', 'Criminal': 'Criminal Contact',
                     'Entertainer': 'By Popular Demand', 'Faceless': 'Dual Personalities',
                     'Faction Agent': 'Safe Haven', 'Failed Merchant': 'Supply Chain',
                     'Far Traveler': 'All Eyes on You', 'Feylost': 'Feywild Connection',
                     'Fisher': 'Harvest the Water', 'Folk Hero': 'Rustic Hospitality',
                     'Gambler': 'Never Tell Me the Odds', 'Gladiator': 'By Popular Demand',
                     'Guild Artisan': 'Guild Membership', 'Haunted One': 'Heart of Darkness',
                     'Hermit': 'Discovery', 'Inheritor': 'Inheritance', 'Investigator': 'Official Inquiry',
                     'Knight': 'Retainers', 'Marine': 'Steady', 'Mercenary Veteran': 'Mercenary Life',
                     'Noble': 'Position of Privilege', 'Outlander': 'Wanderer', 'Pirate': 'Bad Reputation',
                     'Sage': 'Researcher', 'Sailor': 'Ship\'s Passage', 'Shipwright': 'I\'ll Patch It!',
                     'Smugger': 'Down Low', 'Soldier': 'Military Rank', 'Urban Bounty Hunter': 'Ear to the Ground',
                     'Urchin': 'City Secrets', 'Witchlight Hand': 'Carnival Fixture'}

player_modifiers = {0: -5, 1: -5, 2: -4, 3: -4, 4: -3, 5: -3, 6: -2, 7: -2, 8: -1, 9: -1, 10: +0, 11: +0,
                    12: +1, 13: +1, 14: +2, 15: +2, 16: +3, 17: +3, 18: +4, 19: +4, 20: +5}